# Hacker news 🔥

Welcome to this app! I hope that enjoy it and have not any issue to running the services.

# Services 💻

### Postgres database 🔢

```bash
docker-compose up db
```

### Nestjs API 🧮

```bash
docker-compose up api
```
- URL in browser with swagger: http://localhost:3333/api/docs 
- URL in postman: http://localhost:3333 
### Pgadmin database client 👩‍💻

```
docker-compose up pgadmin
```

### Credentials 🔑

Database

```yml
- POSTGRES_USER=root
- POSTGRES_PASSWORD=root
- POSTGRES_DB=hacker_news
```

Admin

```yml
- PGADMIN_DEFAULT_EMAIL=admin@admin.com
- PGADMIN_DEFAULT_PASSWORD=root
```
- URL in browser: http://localhost:5050 

### POSTMAN - Authorization parameter with a JWT
- Send token in header of each endpoint 

```yml
$ # POST /auth/login
$ curl -X POST http://localhost:3000/auth/login -d '{"username": "john", "password": "changeme"}' -H "Content-Type: application/json"
$ # result -> {"access_token":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2Vybm... }
```

```yml
$ # GET /hits using access_token returned from previous step as bearer code
$ curl http://localhost:3000/profile -H "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2Vybm..."
$ # result -> [{ "created_at": "2021-12-24T18:16:09.000Z","title": null, "url": null, "author": "PaulHoule", "points": null, "story_text": null... }]

```

### POSTMAN - FILTER

```yml
$ # POST http://localhost:3333/hits/filter
```
- Body interface for filter
```yml
    //Filters are optional
    {
        "author" : string,
        "title": string,
        "tag" : string[],
        "created_at" : string
    }
    //Created_at : string (name of the month)
    
    // Example
    /* {
        "tag": ["comment", "author_duped"],
        "created_at" : "December"
        }   
    */

```

### Angular client 🖌️
- Status: Developing - without concluding

```bash
docker-compose up client
```
URL in browser: http://localhost:4200 
