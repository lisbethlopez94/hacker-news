import { ConnectionOptions } from 'typeorm';

const config: ConnectionOptions = {
    type: "postgres",
    host: '172.18.0.1',
    port: 5432,
    username: "root",
    password: "root",
    database: "hacker_news",
    synchronize: true,
    entities: ["dist/api/**/*.entity.js"],
    migrations: ["dist/api/migrations/*.js"],
    cli: {
      migrationsDir: "apps/api/migrations"
    }
}

export = config;