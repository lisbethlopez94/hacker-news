import { Module } from '@nestjs/common';
import { HitsModule } from './hits/hits.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Connection } from 'typeorm';
import * as ormconfig from '../../ormconfig';
import {ConfigModule} from "@nestjs/config";
import { ScheduleModule } from '@nestjs/schedule';
import { UsersModule } from './users/users.module';
import { AppController } from './app.controller';
import { AuthModule } from './aut/aut.module';
@Module({
  imports: [    
    ScheduleModule.forRoot(),
    ConfigModule.forRoot(),
    TypeOrmModule.forRoot({...ormconfig, 
      keepConnectionAlive: true, 
      autoLoadEntities: true}),
     HitsModule,
     AuthModule,
     UsersModule
  ],
  controllers: [AppController],
  providers: [],
})
export class AppModule {
  constructor(private connection: Connection) {}
}
