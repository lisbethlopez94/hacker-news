import { Test, TestingModule } from '@nestjs/testing';
import { HitsController } from './hits.controller';

describe('HitsController', () => {
  let controller: HitsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [HitsController],
    }).compile();

    controller = module.get<HitsController>(HitsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
