import { Body, Controller, Delete, Get, HttpException, HttpStatus, Param, Post, UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from '../../../aut/jwt-auth.guard';
import { BodyFilterModel } from '../../interfaces/hits.interface';
import { HitsService } from '../../services/hits/hits.service';

@Controller('hits')
export class HitsController {

    constructor(
        private hitsService: HitsService
      ) {}

    @UseGuards(JwtAuthGuard)
    @Get()
    getAll() {
        return this.hitsService.getAll();
    }

    @UseGuards(JwtAuthGuard)
    @Post('filter')
    filterData(@Body() bodyFilter: BodyFilterModel){
        if(Object.keys(bodyFilter).length === 0 ){
            throw new HttpException('Bad Request', HttpStatus.BAD_REQUEST)
        }else{
            return this.hitsService.filter(bodyFilter)
        }
    }

    @UseGuards(JwtAuthGuard)
    @Delete(':id')
    delete(@Param('id') id: string) {
        return this.hitsService.delete(id)
    }
    
}
