import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';
import { HitsModel } from '../interfaces/hits.interface';

@Entity()
export class Hit implements HitsModel {

  @Column({
    nullable: true
  })
  created_at : Date;

  @Column({
    nullable: true
  })
  title: string | null;

  @Column({
    nullable: true
  })
  url: string | null;

  @Column({
    nullable: true
  })
  author: string;

  @Column({
    nullable: true
  })
  points: string | null;

  @Column({
    nullable: true
  })
  story_text: string | null;

  @Column({
    nullable: true
  })
  comment_text: string;

  @Column({
    nullable: true
  })
  num_comments: number | null;

  @Column({
    nullable: true
  })
  story_id: number;

  @Column({
    nullable: true
  })
  story_title: string;

  @Column({
    nullable: true
  })
  story_url: string;

  @Column({
    nullable: true
  })
  parent_id: number;

  @Column({
    nullable: true
  })
  created_at_i: number;

  @Column({
    nullable: true, type: 'jsonb'
  })
  _tags: any | null

  @Column({
    primary: true
  })
  objectID: string;

  @Column({
    nullable: true,
    type: 'jsonb'
  })
  _highlightResult : any | null;

  @Column('boolean', {default: false})
  deleted: boolean
}