import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { HitsController } from './controllers/hits/hits.controller';
import { Hit } from './entities/hits.entity';
import { HitsService } from './services/hits/hits.service';


@Module({
    imports: [
        HttpModule,
        TypeOrmModule.forFeature([Hit])
    ],
    controllers: [HitsController],
    providers: [HitsService]  
})
export class HitsModule {}
