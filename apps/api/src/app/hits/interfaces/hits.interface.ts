export interface HitsModel {
    created_at : Date | null,
    title: string | null,
    url: string | null,
    author: string,
    points: string | null,
    story_text: string | null,
    comment_text: string,
    num_comments: number | null,
    story_id: number | null,
    story_title: string | null,
    story_url: string | null,
    parent_id: number | null,
    created_at_i: number | null,
    _tags : any | null,
    objectID: string,
    _highlightResult : any | null,
    deleted: boolean
};

export interface BodyFilterModel {
    author? : string,
    title?: string,
    tag? : string[],
    created_at? : string
}
