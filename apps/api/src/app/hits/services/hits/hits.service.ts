import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { Hit } from '../../entities/hits.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Cron, CronExpression } from '@nestjs/schedule';
import { HttpService } from '@nestjs/axios';
import { BodyFilterModel, HitsModel } from '../../interfaces/hits.interface';

@Injectable()
export class HitsService {

    constructor(
        private httpService: HttpService,
        @InjectRepository(Hit) private hitsRepo: Repository<Hit>,
    ) { }

    @Cron(CronExpression.EVERY_HOUR)
    connectApiEveryHour() {
        this.getData();
    }

    getData() {
        return this.httpService.get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs')
            .subscribe(response => {
                let hit: HitsModel[] = response.data.hits;
                hit.map(item => {
                    if(item.deleted !== true){
                        this.saveData(item).then(res =>{
                            console.log('Connecting to API',res); 
                        })
                    }   
                })
            })
    }

    async saveData(data: HitsModel) {
        let instanceHit = await this.hitsRepo.create(data);
        let newHits = await this.hitsRepo.save(instanceHit);
        return newHits
    }

    getAll() {
        return this.hitsRepo.find({ where:{ deleted: false } });
    }

    async filter(bodyFilter: BodyFilterModel) {        
        let sqlBase = "SELECT * FROM hit WHERE "
        sqlBase = this.buildSqlConditions(bodyFilter, sqlBase)
        return this.hitsRepo.query(sqlBase)
              .then( response => {
                    return response
              })
    }

    private buildSqlConditions(bodyFilter: BodyFilterModel, sqlBase: string){
        if(bodyFilter.title){
            if(sqlBase.slice(-6) !== 'WHERE '){
                sqlBase += ' AND '
            };
            sqlBase = sqlBase + ` title like '%${bodyFilter.title}%'`
        }
        if(bodyFilter.author){
            if(sqlBase.slice(-6) !== 'WHERE '){
                sqlBase += ' AND '
            };
            sqlBase = sqlBase + ` author like '%${bodyFilter.author}%'`
        }
        if(bodyFilter.tag && bodyFilter.tag.length > 0){ 
            if(sqlBase.slice(-6) !== 'WHERE '){
                sqlBase += ' AND '
            };
            bodyFilter.tag.map((tag, index) => {
                if(index === 0){
                    sqlBase = sqlBase + ` _tags::text similar to '%${tag}%'`
                }else{
                    sqlBase = sqlBase + " AND " + ` _tags::text similar to '%${tag}%'`
                }
            })
        }
        if(bodyFilter.created_at){
            if(sqlBase.slice(-6) !== 'WHERE '){
                sqlBase += ' AND '
            }
            sqlBase = sqlBase + " to_char(created_at, 'Month') LIKE " + `'%${bodyFilter.created_at}%' `
        }
        sqlBase = sqlBase + " ORDER BY created_at DESC "
        return sqlBase
    }


    async delete(id: string) {        
        return this.hitsRepo.update(
            { objectID: id },
            { deleted: true }
        );
    }

}
