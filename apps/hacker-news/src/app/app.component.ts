import { Component } from '@angular/core';

@Component({
  selector: 'hacker-news-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
}
