import { AfterViewInit, Component, OnInit } from '@angular/core';
import { BodyFilterModel, HitsModel } from 'apps/api/src/app/hits/interfaces/hits.interface';
import { Observable } from 'rxjs';
import { HitsFService } from '../hitsf.service';


@Component({
  selector: 'hacker-news-hits',
  templateUrl: './hits.component.html',
  styleUrls: ['./hits.component.scss']
})
export class HitsComponent implements OnInit {

  news!: HitsModel[];
  filterBody!: BodyFilterModel;

  constructor(
    public hitsFService : HitsFService
    ) { }

  ngOnInit(): void {
    this.getNews()
  }

  getNews() {
    this.hitsFService.getAllHits()
    .subscribe((res : HitsModel[]) => {this.news = res});
  }
  deleteNew(id: string) {
    this.hitsFService.deleteHit(id)
    .subscribe(() => {this.getNews()});
  }
  
  filter(){
    this.hitsFService.filterHits(this.filterBody)
    .subscribe((res : HitsModel[]) => {this.news = res});
  }

}
