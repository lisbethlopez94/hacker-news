import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HitsComponent } from './components/hits.component';

@NgModule({
  declarations: [
    HitsComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    HitsComponent
  ]
})
export class HitsModule { }
