import { TestBed } from '@angular/core/testing';
import { HitsFService } from './hitsf.service';

describe('HitsService', () => {
  let service: HitsFService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HitsFService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
