import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "apps/hacker-news/src/environments/environment";
import { Observable } from "rxjs";
import { HitsModel, BodyFilterModel } from '../../../../../api/src/app/hits/interfaces/hits.interface';


@Injectable({
  providedIn: 'root'
})
export class HitsFService {

  constructor(private http: HttpClient) { }

  getAllHits() {
    return this.http.get<HitsModel[]>(`${environment.apiUrl}/hits`);
  }

  filterHits(bodyFilter: BodyFilterModel) {
    return this.http.post<HitsModel[]>(`${environment.apiUrl}/hits/filter`, bodyFilter);
  }

  deleteHit(id: string) {
    return this.http.delete(`${environment.apiUrl}/hits/${id}`);
  }

}
